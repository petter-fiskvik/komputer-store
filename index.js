const express = require("express")
const app = express()
const {PORT = 3000} = process.env
const path = require("path")

// Default Route for your website
app.get("/", (req, res) => {
    res.sendFile(path.join (__dirname, "public", "index.html" ))
})

app.listen(PORT, () => console.log(`Server is running on ${PORT}`))

// Tell express to allow access to static content
app.use(express.static(path.join(__dirname, "public", "static")))