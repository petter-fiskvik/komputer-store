// Dynamic elements
const balanceElement = document.getElementById("balance")
const loanElement = document.getElementById("loan")
const payElement = document.getElementById("pay")
const computersElement = document.getElementById("computers")
const specsElement = document.getElementById("specs")
const priceElement = document.getElementById("price")
const descriptionElement = document.getElementById("description")
const loanRowElement = document.getElementById("loan-row")
const imgElement = document.getElementById("img")
const nameElement = document.getElementById("name")

// Buttons
const loanButton = document.getElementById("loan-btn")
const bankButton = document.getElementById("bank-btn")
const workButton = document.getElementById("work-btn")
const payButton = document.getElementById("pay-btn")
const buyButton = document.getElementById("buy-btn")

// ...
const baseImgURL = "https://noroff-komputer-store-api.herokuapp.com/"
const currency = " kr"
const currencyCaps = " NOK"

let balance = 1000.0
let loan = 0.0
let pay = 0.0
let computers = []

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputers(computers))
    .finally(() => hydrate())

const addComputers = (computers) => {
    computers.forEach( c => addComputer(c))
}

const addComputer = (computer) => {
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    computersElement.appendChild(computerElement)
}

// Set dynamic elements to display data related to the first computer in our <select>
// by dispatching a change event
const hydrate = () => {
    const e = new Event("change")
    e.target = 0
    computersElement.dispatchEvent(e)
}

// Event listeners and functions
const handleComputerChange = e => {
    const selectedComputer = computers[e.target.selectedIndex]
    priceElement.innerText = selectedComputer.price
    const specs = selectedComputer.specs
    let specParagraph = ""
    for (let i = 0 ; i < specs.length ; i++) {
        specParagraph += specs[i]
        if (i < specs.length - 1) {
            specParagraph += "\n"
        }
    }
    specsElement.innerText = specParagraph
    imgElement.src = baseImgURL + selectedComputer.image
    imgElement.alt = selectedComputer.title
    nameElement.innerText = selectedComputer.title
    descriptionElement.innerText = selectedComputer.description

    price = selectedComputer.price
    priceElement.innerText = price + currencyCaps
}
computersElement.addEventListener("change", handleComputerChange)

const work = () => {
    pay += 100
    payElement.innerText = pay + currency
}
workButton.addEventListener("click", work)

const bank = () => {
    // If the user has a loan, 10% of their pay goes towards paying that first
    if (loan > 0) {
        // Make sure the user does not pay back more than they owe
        if (loan < pay * 0.1) {
            balance += pay - loan
            balanceElement.innerText = balance + currency
            loan = 0
            loanElement.innerText = loan + currency
            pay = 0
            payElement.innerText = pay + currency
        } else {
            loan -= pay * 0.1
            loanElement.innerText = loan + currency
            balance += pay * 0.9
            balanceElement.innerText = balance + currency
            pay = 0
            payElement.innerText = pay + currency
        }

        // No more debts? remove the row nad "pay" button
        if (loan <= 0) {
            loanRowElement.style.display = "none"
            payButton.style.display = "none"
        }
        // No loan, full pay transfered to bank
    } else {
        balance += pay
        balanceElement.innerText = balance + currency
        pay = 0
        payElement.innerText = pay + currency
    }
}
bankButton.addEventListener("click", bank)

const getLoan = () => {
    // Make sure the user has no outstanding debts
    if (loan == 0) {
        // Explicit coercion is necessary
        const loanApplication = parseFloat(prompt(`
            How much would you like to loan?
            you can take out a loan of up to twice your current balance
        `))

        // Catch bad input
        if (loanApplication == NaN) {
            alert("Numbers only, would you kindly")
            //
        } else if (loanApplication > balance * 2) {
            alert("You can only take out a loan of up to twice your current balance")
        } else {
            balance += loanApplication
            balanceElement.innerText = balance + currency
            loan = loanApplication
            loanElement.innerText = loan + currency
            loanRowElement.style.display = "flex"
            payButton.style.display = "block"
        }
    } else {
        alert("Existing loans must be paid in full before further loans can be approved")
    }
}
loanButton.addEventListener("click", getLoan)

const buy = () => {
    if (price > balance) {
        alert("Insufficient balance")
    } else {
        if (confirm("Confirm purchase")) {
            balance -= price
            balanceElement.innerText = balance + currency
            alert("Thank you for your business")
        }
    }
}
buyButton.addEventListener("click", buy)

const payLoan = () => {
    // If there is more debt than pay...
    if (loan > pay) {
        loan -= pay
        loanElement.innerText = loan + currency
        pay = 0
        payElement.innerText = pay + currency
        // ... else this will be the last payment
    } else {
        pay -= loan
        payElement.innerText = pay + currency
        loan = 0
        loanElement.innerText = loan + currency

        payButton.style.display = "none"
        loanRowElement.style.display = "none"
    }
}
payButton.addEventListener("click", payLoan)